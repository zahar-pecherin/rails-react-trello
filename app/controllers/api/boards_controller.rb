class Api::BoardsController < Api::BaseController
  skip_before_action :verify_authenticity_token

  def index
    @owned_boards = current_user.owned_boards.order(created_at: :desc)
    @invited_boards = Board.not_owned_by(current_user)

    render json: {owned_boards: @owned_boards,
                  invited_boards: @invited_boards}
  end

  def create
    @board = current_user.owned_boards.new(board_params)

    if @board.save
      render json: @board
    else
      render json: @board.errors, status: :unprocessable_entity
    end
  end

  private

  def board_params
    params.require(:board).permit(:name)
  end
end
