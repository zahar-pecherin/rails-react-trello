class Api::PeopleController < Api::BaseController
  def index; end

  def get_current_user
    @user = current_user
    render json: @user
  end
end
