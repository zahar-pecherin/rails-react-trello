import { combineReducers }  from 'redux';
import { routerReducer }    from 'react-router-redux';
import registration         from './registration';
import session              from './session';
import boards               from './boards';
import header               from './header';

export default combineReducers({
  routing: routerReducer,
  registration: registration,
  session: session,
  boards: boards,
  header: header,
});
