import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware }             from 'react-router-redux';
import { composeWithDevTools }          from 'redux-devtools-extension';
import thunkMiddleware                  from 'redux-thunk';
import reducers                         from '../reducers';

export default function configureStore(browserHistory) {
  const reduxRouterMiddleware = routerMiddleware(browserHistory);

  const createStoreWithMiddleware = createStore(reducers, composeWithDevTools(applyMiddleware(thunkMiddleware, reduxRouterMiddleware)));

  return createStoreWithMiddleware;
}
