import React            from 'react';
import { connect }      from 'react-redux';
import { Link }         from 'react-router';
import ReactGravatar    from 'react-gravatar';
import { push }         from 'react-router-redux';

import SessionActions   from '../actions/sessions';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.renderCurrentUser = this.renderCurrentUser.bind(this);
    this.renderSignOutLink = this.renderSignOutLink.bind(this);
    this.handleSignOutClick = this.handleSignOutClick.bind(this);
  }

  renderCurrentUser() {
    const { currentUser } = this.props;

    if (!currentUser) {
      return false;
    }
    const fullName = [currentUser.first_name, currentUser.last_name].join(' ');

    return (
      <a className="current-user">
        <ReactGravatar className="react-gravatar" email={currentUser.email} /> {fullName}
      </a>
    );
  }

  renderSignOutLink() {
    if (!this.props.currentUser) {
      return false;
    }
    return (
      <a href="#" onClick={this.handleSignOutClick}><i className="fa fa-sign-out"/> Sign out</a>
    );
  }

  handleSignOutClick(e) {
    e.preventDefault();

    this.props.dispatch(SessionActions.signOut());
  }

  render() {
    return (
      <header className="main-header">
        <nav id="boards_nav">
          <ul>
            <li>
              <a href="#" onClick={this.handleBoardsClick}><i className="fa fa-columns"/> Boards</a>
              {/*{this.renderBoards()}*/}
            </li>
          </ul>
        </nav>
        <Link to='/'>
          <span className='logo'/>
        </Link>
        <nav className="right">
          <ul>
            <li>
              {this.renderCurrentUser()}
            </li>
            <li>
              {this.renderSignOutLink()}
            </li>
          </ul>
        </nav>
      </header>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.session.currentUser,
  header: state.header,
});

export default connect(mapStateToProps)(Header);
