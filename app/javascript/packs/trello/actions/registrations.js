import { push }           from 'react-router-redux';
import Constants          from '../constants';
import axios              from 'axios';
import {setCurrentUser}   from './sessions';

const Actions = {};

Actions.signUp = (data) => {
  return (dispatch) => {
    axios.post('/users', { user: data })
      .then(function (response) {
        if (response.data.user.id) {
          localStorage.setItem('railsAuthToken', response.data.user.id);
          setCurrentUser(dispatch, response.data.user);
          dispatch(push('/'));
        } else {
          dispatch({
            type: Constants.REGISTRATIONS_ERROR,
            errors: response.data.errors,
          });
          dispatch(push('/sign_up'));
        }
      })
      .catch(function (error) {
        console.log(error);
      })
  };
};

export default Actions;
