import Constants              from '../constants';
import { push }               from 'react-router-redux';
// import CurrentBoardActions    from './current_board';
import axios              from 'axios';

const Actions = {
  fetchBoards: () => {
    return dispatch => {
      dispatch({ type: Constants.BOARDS_FETCHING });

      axios.get('/api/boards')
      .then((response) => {
        dispatch({
          type: Constants.BOARDS_RECEIVED,
          ownedBoards: response.data.owned_boards,
          invitedBoards: response.data.invited_boards,
        });
      });
    };
  },

  showForm: (show) => {
    return dispatch => {
      dispatch({
        type: Constants.BOARDS_SHOW_FORM,
        show: show,
      });
    };
  },

  create: (data) => {
    return dispatch => {
      axios.post('/api/boards', { board: data })
      .then((response) => {
        dispatch({
          type: Constants.BOARDS_NEW_BOARD_CREATED,
          board: response.data,
        });
        dispatch({
          type: Constants.BOARDS_SHOW_FORM,
          show: false,
        });
        // dispatch(push(`/boards/${data.id}`));
      })
      .catch((error) => {
        dispatch({
          type: Constants.BOARDS_CREATE_ERROR,
          errors: "Don't create",
        });
      });
    };
  },

  reset: () => {
    return dispatch => {
      dispatch({
        type: Constants.BOARDS_RESET,
      });
    };
  },
};

export default Actions;
