import Constants              from '../constants';
import { push }               from 'react-router-redux';

const Actions = {
  showBoards: (show) => {
    return dispatch => {
      dispatch({
        type: Constants.HEADER_SHOW_BOARDS,
        show: show,
      });
    };
  },

};

export default Actions;
