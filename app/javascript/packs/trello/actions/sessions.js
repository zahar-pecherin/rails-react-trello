import { push }                           from 'react-router-redux';
import Constants                          from '../constants';
import axios                              from 'axios';

export function setCurrentUser(dispatch, user) {
  dispatch({
    type: Constants.CURRENT_USER,
    currentUser: user,
  });
};

const Actions = {
  signIn: (email, password) => {
    return dispatch => {
      const data =  {user: {
        email: email,
        password: password,
        }
      };

      axios.post('/users/sign_in', data)
        .then((response) => {
          localStorage.setItem('railsAuthToken', response.data.id);
          setCurrentUser(dispatch, response.data);
          dispatch(push('/'));
        })
        .catch((error) => {
          dispatch({
            type: Constants.SESSIONS_ERROR,
            error: 'Invalid email or password',
          });
        });
    };
  },

  currentUser: () => {
    return dispatch => {
      const authToken = localStorage.getItem('railsAuthToken');
      axios.get('/api/people/get_current_user')
        .then(function (response) {
          setCurrentUser(dispatch, response.data);
        })
        .catch(function (error) {
          console.log(error);
          dispatch(push('/sign_in'));
        });
    };
  },

  signOut: () => {
    return dispatch => {
      axios.delete('/users/sign_out')
        .then((data) => {
          localStorage.removeItem('railsAuthToken');

          dispatch({ type: Constants.USER_SIGNED_OUT, });

          dispatch(push('/sign_in'));

          // dispatch({ type: Constants.BOARDS_FULL_RESET });
        })
        .catch(function (error) {
          console.log(error);
        });
    };
  },
};

export default Actions;