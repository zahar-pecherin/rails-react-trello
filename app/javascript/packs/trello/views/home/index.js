import React                from 'react';
import { connect }          from 'react-redux';
import classnames           from 'classnames';

import { setDocumentTitle } from '../../utils';
import Actions              from '../../actions/boards';
import BoardCard            from '../../components/boards/card';
import BoardForm            from '../../components/boards/form';

class HomeIndexView extends React.Component {
  constructor(props) {
    super(props);
    this.handleAddNewClick = this.handleAddNewClick.bind(this);
    this.handleCancelClick = this.handleCancelClick.bind(this);
    this.renderOwnedBoards = this.renderOwnedBoards.bind(this);
    this.renderAddNewBoard = this.renderAddNewBoard.bind(this);
    this.renderAddButton = this.renderAddButton.bind(this);
    this.renderAddNewBoard = this.renderAddNewBoard.bind(this);
    this.renderOtherBoards = this.renderOtherBoards.bind(this);
  }

  componentDidMount() {
    setDocumentTitle('Boards');
  }

  componentWillUnmount() {
    this.props.dispatch(Actions.reset());
  }

  renderOwnedBoards() {
    const { fetching } = this.props;

    let content = false;

    const iconClasses = classnames({
      fa: true,
      'fa-user':    !fetching,
      'fa-spinner': fetching,
      'fa-spin':    fetching,
    });

    if (!fetching) {
      content = (
        <div className="boards-wrapper">
          {this.renderBoards(this.props.ownedBoards)}
          {this.renderAddNewBoard()}
        </div>
      );
    }

    return (
      <section>
        <header className="view-header">
          <h3><i className={iconClasses} /> My boards</h3>
        </header>
        {content}
      </section>
    );
  }
  //
  renderBoards(boards) {
    return boards.map((board) => {
      return <BoardCard
        key={board.id}
        dispatch={this.props.dispatch}
        {...board} />;
    });
  }

  renderAddNewBoard() {
    let { showForm, dispatch, formErrors } = this.props;

    if (!showForm) return this.renderAddButton();

    return (
      <BoardForm
        dispatch={dispatch}
        errors={formErrors}
        onCancelClick={this.handleCancelClick}/>
    );
  }

  renderOtherBoards() {
    const { invitedBoards } = this.props;

    if (invitedBoards.length === 0) return false;

    return (
      <section>
        <header className="view-header">
          <h3><i className="fa fa-users" /> Other boards</h3>
        </header>
        <div className="boards-wrapper">
          {this.renderBoards(invitedBoards)}
        </div>
      </section>
    );
  }

  renderAddButton() {
    return (
      <div className="board add-new" onClick={this.handleAddNewClick}>
        <div className="inner">
          <a id="add_new_board">Add new board...</a>
        </div>
      </div>
    );
  }

  handleAddNewClick() {
    let { dispatch } = this.props;
    dispatch(Actions.showForm(true));
  }

  handleCancelClick() {
    this.props.dispatch(Actions.showForm(false));
  }

  render() {
    return (
      <div className="view-container boards index">
        Home Index
        {this.renderOwnedBoards()}
        {this.renderOtherBoards()}
      </div>
    );
  }
}

const mapStateToProps = (state) => (
  state.boards
);

export default connect(mapStateToProps)(HomeIndexView);
