import React, {PropTypes}   from 'react';
import { connect }          from 'react-redux';

import Constants            from '../../constants';
import { setDocumentTitle } from '../../utils';




class BoardsShowView extends React.Component {
  componentDidMount() {

  }

  componentWillUpdate(nextProps, nextState) {

  }

  componentWillUnmount() {

  }



  render() {

    return (
      <div className="view-container boards show">
        <div>

        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  currentUser: state.session.currentUser,
});

export default connect(mapStateToProps)(BoardsShowView);
