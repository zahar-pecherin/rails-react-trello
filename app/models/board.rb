class Board < ApplicationRecord
  belongs_to :user
  has_many :users_boards
  has_many :members, through: :users_boards, foreign_key: "user_id"

  validates :name, presence: true

  scope :not_owned_by, -> (user) {where.not(user_id: user.id)}
end
