Rails.application.routes.draw do
  devise_for :users, defaults: { format: :json }, controllers: { sessions: 'users/sessions', registrations: 'users/registrations' }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: "home#index"

  get '/sign_up', to: "home#index"
  get '/sign_in', to: "home#index"
  delete '/', to: "home#index"
  get '/boards/:id', to: "home#index"

  # API
  namespace :api do
    resources :people, only: [:index] do
      collection do
        get 'get_current_user'
      end
    end
    resources :boards, only: [:index, :create]
  end
end
