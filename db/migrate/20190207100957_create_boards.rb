class CreateBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :boards do |t|
      t.references :user
      t.string :name, null: false

      t.timestamps
    end
  end
end
