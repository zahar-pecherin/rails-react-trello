class CreateUsersBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :users_boards do |t|
      t.references :user
      t.references :board

      t.timestamps
    end
  end
end
